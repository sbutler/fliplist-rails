# frozen_string_literal: true

# == Schema Information
#
# Table name: accounts
#
#  id           :bigint(8)        not null, primary key
#  name         :string
#  nickname     :string
#  image        :string
#  description  :string
#  provider     :string           default("twitter")
#  provider_uid :string
#  token        :string
#  secret       :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Account < ApplicationRecord
  validates :provider_uid, uniqueness: true

  # rubocop:disable Metrics/AbcSize
  def self.create_from_omniauth(auth)
    create! do |account|
      account.name = auth[:info][:name]
      account.nickname = auth[:info][:nickname]
      account.image = auth[:info][:image]
      account.description = auth[:info][:description]
      account.provider_uid = auth[:uid]
      account.token = auth[:credentials][:token]
      account.secret = auth[:credentials][:secret]
    end
    # rubocop:enable Metrics/AbcSize
  end
end
