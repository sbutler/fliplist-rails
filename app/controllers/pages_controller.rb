# frozen_string_literal: true

class PagesController < ApplicationController
  def show
    @page = params[:id].tr('-', '_')
  end
end
