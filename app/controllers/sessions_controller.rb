# frozen_string_literal: true

class SessionsController < ApplicationController
  def create
    account = Account.where(provider_uid: auth['uid']).first

    if account
      account.update(
        name: auth[:info][:name],
        nickname: auth[:info][:nickname],
        image: auth[:info][:image],
        description: auth[:info][:description],
        token: auth[:credentials][:token],
        secret: auth[:credentials][:secret])
    else
      account = Account.create_from_omniauth(auth)
    end

    session['account_id'] = account.id
    redirect_to new_retweet_path
  end

  def destroy
    session['account_id'] = nil
    redirect_to root_url, notice: 'Signed Out!'
  end

  private

  def auth
    request.env['omniauth.auth']
  end
end
