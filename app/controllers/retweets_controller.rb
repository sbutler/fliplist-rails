# frozen_string_literal: true

class RetweetsController < ApplicationController
  def new
    return unless current_account

    Retweeter.new(current_account).send
    session[:account_id] = nil

    redirect_to page_path('success'), notice: 'Live Long and Prosper!'
  end
end
