# frozen_string_literal: true

class ApplicationController < ActionController::Base
  private

  def current_account
    account_id = session[:account_id]
    @current_account ||= Account.find(account_id) if account_id
  end
  helper_method :current_account
end
