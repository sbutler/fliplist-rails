# frozen_string_literal: true

class FavoriteFetcher
  def initialize
    @client = Twitter::REST::Client.new do |config|
      config.consumer_key = Rails.application.secrets.twitter_key
      config.consumer_secret = Rails.application.secrets.twitter_secret
      config.access_token = Rails.application.secrets.twitter_account_key
      config.access_token_secret = Rails.application.secrets.twitter_account_secret
    end
  end

  def all
    @client.favorites
  end
end
