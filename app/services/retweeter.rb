# frozen_string_literal: true

class Retweeter
  def initialize(account)
    @client = Twitter::REST::Client.new do |config|
      config.consumer_key = Rails.application.secrets.twitter_key
      config.consumer_secret = Rails.application.secrets.twitter_secret
      config.access_token = account.token
      config.access_token_secret = account.secret
    end
  end

  def send
    tweets.each do |tweet|
      @client.retweet(tweet.id)
    end
  end

  private

  def tweets
    @tweets ||= FavoriteFetcher.new.all
  end
end
