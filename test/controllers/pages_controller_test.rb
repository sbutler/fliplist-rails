# frozen_string_literal: true

require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  class Show < PagesControllerTest
    test 'get privacy policy as guest' do
      get page_path('privacy-policy')

      assert_response :success
      assert_title 'Privacy Policy'
    end

    test 'get terms as guest' do
      get page_path('terms-of-use')

      assert_response :success
      assert_title 'Terms of Use'
    end
  end
end
