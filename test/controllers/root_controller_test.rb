# frozen_string_literal: true

require 'test_helper'

class RootControllerTest < ActionDispatch::IntegrationTest
  test 'GET as guest' do
    get root_url

    assert_response :success
    assert_equal 'show', @controller.action_name
    assert_equal 'root', @controller.controller_name
    assert_title 'Empowering voices. Flipping issues. Moving the world.'
  end
end
