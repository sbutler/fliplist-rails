# frozen_string_literal: true

# == Schema Information
#
# Table name: accounts
#
#  id           :bigint(8)        not null, primary key
#  name         :string
#  nickname     :string
#  image        :string
#  description  :string
#  provider     :string           default("twitter")
#  provider_uid :string
#  token        :string
#  secret       :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  def setup
    @account = accounts(:one)
  end

  test 'sanity' do
    assert @account.valid?
  end

  test 'uniqueness of provider_uid' do
    second_account = accounts(:two)
    assert second_account.valid?

    second_account.provider_uid = @account.provider_uid
    assert_not second_account.valid?
  end
end
