# frozen_string_literal: true

require 'application_system_test_case'

class SignInWithTwitterTest < ApplicationSystemTestCase
  test 'signing in' do
    visit root_url
    click_on 'Sign in with Twitter'
  end
end
