# FlipList

## Adding Static Pages

Create a new file in `app/views/pages`. The filename must start with an
underscore and each word in the filename should be seperated by an
underscore, and have an extension of `.html.erb`.

```
app/views/pages/_about_us.html.erb
```

The contents of the file should include HTML and provide an title.

```erb
<h1>About Us</h1>

<p>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
  laoreet mauris suscipit urna laoreet ultricies.
</p>

<% provide :title, 'About Us' %>
```

The page can then be accessed at `http://url/pages/about-us`. the name
which is used in the url is the same as the file name, without the leading
underscore and all other underscorse changed to dashes and without an
extension.

### Linking to static pages

Create links using the `link_to` rails helper taking advantage of the
route helper `page_path`. The string that you pass to `page_path` should
be the same as the filename without the leading underscore, all other underscores changed to dashes and without the extension.

```erb
  <%= link_to 'About Us', page_path('about-us') %>
```