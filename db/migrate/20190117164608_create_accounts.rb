class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :name
      t.string :nickname
      t.string :image
      t.string :description
      t.string :provider, default: 'twitter'
      t.string :provider_uid
      t.string :token
      t.string :secret

      t.timestamps
    end

    add_index :accounts, :provider_uid, unique: true
  end
end
