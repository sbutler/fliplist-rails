Rails.application.routes.draw do
  resources :pages, only: :show
  resources :retweets, only: :new

  root to: 'root#show'

  get 'auth/twitter/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: :signout
end
